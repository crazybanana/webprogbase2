const express = require("express");
const bodyParser = require("body-parser");
const multer = require('multer');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const paginate = require("paginate-array");
const mongoose = require("mongoose");
const cities = require("all-the-cities");

//js
const User = require('./model/User');
const Storage = require("./model/Storage");
const config = require("./lib/config");

const app = express();
const upload  = multer();
const country = cities.filter(city =>{
    return city.country.match('UA');
});
const cityBuff = country.map(a => a.name);

//database
mongoose.connect("mongodb://goods:Crazybanana123@ds149535.mlab.com:49535/heroku_8zs60hrv", {
    useMongoClient: true
});
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('Connected to mongodb successfully.')
});

//server setups
app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(session({
    secret: "webtj947tb$*$)974e",
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

//routes
const goods = require("./routes/goods");
const profile = require('./routes/profile');
const api_goods = require('./routes/api/v1/goods/api');
const api_profile = require('./routes/api/v1/profile/api');
const api = require('./routes/api/v1/api');

app.use('/api/v1/goods', api_goods);
app.use('/api/v1/profile', api_profile);
app.use('/api/v1', api);
app.use('/goods', goods);
app.use('/profile', profile);

//server requests
app.get("/",
    async (req, res) => {
        let storage = await Storage.find();
        res.render('index', {
            user: req.user,
            object: storage, 
            countries: cityBuff
        })
    });

app.get("/search",
    upload.any(),
    async (req, res) => {
        let storage = await Storage.find();
        let filter = [];
        let categoryFilter = [];
        let cityFilter = [];
        let lastFilter = [];

        storage.forEach(function (object) {
            if(((object.name).toLowerCase()).indexOf((req.query.search).toLowerCase()) !== -1){
                filter.push(object);
            }
        });
        lastFilter = filter;
        if(req.query.formCategory !== 'Категория'){
            filter.forEach(function(object){
                if(object.category === req.query.formCategory){
                    categoryFilter.push(object);
                }
            });
            lastFilter = categoryFilter;
        }
        if(req.query.formCity !== 'Город' && req.query.formCategory !== 'Категория'){
            categoryFilter.forEach(function(object){
                if(object.city === req.query.formCity){
                    cityFilter.push(object);
                }
            });
            lastFilter = cityFilter;
        }else if(req.query.formCity !== 'Город'){
            filter.forEach(function(object){
                if(object.city === req.query.formCity){
                    cityFilter.push(object);
                }
            });
            lastFilter = cityFilter;
        }

        let itemsCount = lastFilter.length;
        let pageSize = 20;
        let pagesCount = Math.ceil(itemsCount / pageSize);
        let currentPage = 1;
        let itemsArrays = [];
        let itemsList = [];

        while(lastFilter.length > 0){
            itemsArrays.push(lastFilter.splice(0, pageSize));
        }

        if (typeof req.query.page !== 'undefined') {
             currentPage = +req.query.page;
        }

        itemsList = itemsArrays[+currentPage - 1];

        if(itemsCount !== 0){
            res.render("filter", {
                founded: true,
                user: req.user,
                searchParam: req.query,
                object: itemsList,
                pageSize: pageSize,
                pagesCount: pagesCount,
                totalItems: itemsCount,
                currentPage: currentPage, 
                countries: cityBuff
            });
        }else{
            res.render("filter", {searchParam: req.query,
                founded: false,
                user: req.user, 
                countries: cityBuff});
        }
    });

app.post("/ban",
    upload.any(),
    async (req, res) => {
        let note = {};
        let ban = "ban";
        note[ban] = "true";
        await User.update({_id: req.body.id}, note, (err, result) => {
            if (err) {
                res.send({'error':'storage update error'});
            } else {
                res.redirect('/profile/admin');
            } 
        });
    });

app.use((req, res) => {
    res.render('errors/404', {user: req.user, countries: cityBuff});
});

app.listen(config.port, () => console.log("UP!"));