const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

let storageSchema = new Schema({
    id: {
        type: String
    },
    name: {
        type: String,
        required: true,
    },
    city: {
      type: String,
      required: true
    },
    category: {
        type: String,
        required: true
    },
    condition: {
        type: String,
        required: true
    },
    about: {
        type: String
    },
    image: {
        type: Array
    },
    imageID: {
        type: Array
    },
    ownerID:{
        type: String
    }
});

storageSchema.plugin(mongoosePaginate);

let Storage = mongoose.model("Storage", storageSchema);

module.exports = Storage;