const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

let userSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true        
    },
    email: {
        type: String,
        unique: true,
        required: true        
    },
    phone: {
        type: String,
        unique: true,
        required: true        
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String
    },
    ban: {
        type: Boolean
    }
});

userSchema.plugin(mongoosePaginate);

let User = mongoose.model("User", userSchema);

module.exports = User;