let myInit = {method: 'GET'};

fetch('/api/v1/goods/', myInit)
    .then(function(response) { return response.json(); })
    .then(function(json) {
        let goods = json.docs;
        let link = 'link';
        goods.forEach(element => {
            element[link] = '/goods/' + element.id;
        });
        let template = document.getElementById("my-list-template").innerHTML;
        let renderedHTML = Mustache.render(template, { list: goods});
        document.getElementById("result").innerHTML = renderedHTML;
    });