$(document).ready(function () {

    $("#btnSubmit").click(function (event) {

        event.preventDefault();

        let form = $('#deleteForm')[0];

        let data = new FormData(form);

        $("#btnSubmit").prop("disabled", true);
        
        $.ajax({
            type: "DELETE",
            enctype: 'multipart/form-data',
            url: ("/api/v1/goods/" + $('#btnSubmit').attr('value')),
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {

                // $("#status").text('Loaded');
                // $("#btnSubmit").prop("disabled", false);

                $(location).attr('href', '/');

            },
            error: function (e) {

                $("#status").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);

            }
        });

    });

});