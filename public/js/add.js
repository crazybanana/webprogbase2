$(document).ready(function () {

    $("#btnSubmit").click(function (event) {

        event.preventDefault();

        let form = $('#addform')[0];

        let data = new FormData(form);

        let fp = $('#file');
        let lg = fp[0].files.length;

        $("#btnSubmit").prop("disabled", true);

        if(lg > 8){
            $('#status').text('Вы не можете загрузить больше 8 фотографий');
            $("#btnSubmit").prop("disabled", false);
        }else{
            $.ajax({
                type: "POST",
                enctype: 'multipart/form-data',
                url: "/api/v1/goods/",
                data: data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                success: function (data) {
    
                    $(location).attr('href', '/');
    
                },
                error: function (e) {
    
                    $("#status").text(e.responseText);
                    console.log("ERROR : ", e);
                    $("#btnSubmit").prop("disabled", false);
    
                }
            });
        }
    });
});