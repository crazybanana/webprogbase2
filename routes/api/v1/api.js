const express = require("express");
const multer = require('multer');
const router = express.Router();

let upload  = multer({ storage: multer.memoryStorage() });

router.use(express.static("public"));

router.get('/', 
    async(req, res) =>{
        try{
            res.render('api');            
        }catch(err){

        }
    });

module.exports = router;