const express = require("express");
const cloudinary = require('cloudinary');
const passport = require('passport');
const session = require('express-session');
const LocalStrategy = require('passport-local').Strategy;
const multer = require('multer');
const auth = require('basic-auth');
const router = express.Router();

const Storage = require("../../../../model/Storage");

let upload  = multer({ storage: multer.memoryStorage() });

cloudinary.config({
    cloud_name: 'ddbvrf8lf',
    api_key: '544275783565819',
    api_secret: 'c6e5peIp3c3cjwLh5U7opTSSAps'
});

router.use(express.static("public"));
router.use(session({
    secret: "webtj947tb$*$)974e",
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());

router.get('/', 
    async(req, res) =>{
        try{
            await Storage.paginate({}, {limit: 5}, function(err, result){
                if(err){
                    res.send({'error': 'storage error'})
                }else{
                    res.send(result);                        
                }
            });
        }catch(err){
            res.send({'error': 'catch error'});
        }
    });

router.post("/",
    upload.any(),
    async (req, res) => {
        if(!req.user){
            res.send({'error': 'not authorized'});
        }else{
            try {
                let name, city, category, condition, about, id;
                let ownerID = req.user._id;
                let field = [];
                let image = [];
                let imageID = [];

                if(req.body.name){
                    name = req.body.name;
                    id = claimId(name);
                }else{
                    field.push('name');
                }
                if(req.body.city) city = req.body.city;
                else field.push('city');
                if(req.body.category) category = req.body.category;
                else field.push('category');
                if(req.body.condition) condition = req.body.condition;
                else field.push('condition');
                if(req.body.about) about = req.body.about
                else about = '';

                if(req.files.length > 0){
                    for(let i = 0; i < req.files.length; i++){
                        let base64String = await ('data:image/png;base64,' + (req.files[i].buffer).toString('base64'));
                        await cloudinary.v2.uploader.upload(base64String, function(error, result) {                        
                            image.push(result.url);
                            imageID.push(result.public_id);
                        });
                    }
                }else {            
                    image = null;
                    imageID = null;
                }
    
                let storage = await (new Storage({
                    id,
                    name,
                    city,
                    category,
                    condition,
                    about,
                    image,
                    imageID,
                    ownerID
                }));

                if(field.length > 0){
                    res.send({'Required fields is missing': field}) 
                }else{
                    await storage.save(function (err) {
                        if(err){
                            res.send({'error': 'storage save error'});
                            return;
                        }
                    });
                    res.send(storage);
                }          
            }catch (err){
                res.send({'error': 'catch error'});
            }
        }
    });

router.get('/:good_id',
    upload.any(),
    async(req, res) =>{
        try {
            await Storage.findOne({id: req.params.good_id}, (err, item) => {
                if (err) {
                    res.send({'error':'storage find error'});
                } else {
                    res.send(item);
                } 
            });
        }catch (err){
            res.send({'error': 'catch error'});
        }
    });

router.delete('/:good_id',
    upload.any(),
    async(req, res) =>{
        // let credentials = auth(req);        
        // if(!credentials || credentials.name !== 'Administrator' || credentials.pass !== '1234567890'){
        //     res.send({'error': 'not authorized'});
        // }else{
            try {
                await Storage.findOne({id: req.params.good_id}, async (err, storage) => {
                    if(err) throw err;
                    for(let i = 0; i < storage.imageID.length; i++){
                        if(storage.imageID[i] !== null){
                            await cloudinary.v2.uploader.destroy(storage.imageID[i]);                    
                        }
                    }
                });
                await Storage.remove({id: req.params.good_id}, (err, item) => {
                    if (err) {
                        res.send({'error':'storage remove error'});
                    } else {
                        res.send({'status': 'deleted'});
                    } 
                });
            }catch (err){
                res.send({'error': 'catch error'});
            }
        // }
    });

router.put('/:good_id',
    upload.any(),
    async(req, res) =>{
        let credentials = auth(req);        
        if(!credentials || credentials.name !== 'Administrator' || credentials.pass !== '1234567890'){
            res.send({'error': 'not authorized'});
        }else{
            let note = {};
            try {
                if(req.body.name){
                    let name = "name";
                    let id = "id";
                    note[name] = req.body.name;
                    note[id] = claimId(req.body.name);
                }
                if(req.body.city){
                    let city = "city";
                    note[city] = req.body.city;
                }
                if(req.body.category){
                    let category = "category";
                    note[category] = req.body.category;
                }
                if(req.body.condition){
                    let condition = "condition";
                    note[condition] = req.body.condition;
                }
                if(req.body.about){
                    let about = "about";
                    note[about] = req.body.about;
                }
    
                await Storage.update({id: req.params.good_id}, note, (err, result) => {
                    if (err) {
                        res.send({'error':'storage update error'});
                    } else {
                        res.send(note);
                    } 
                });
    
            }catch (err){
                res.send({'error': 'catch error'});
            }
        }
    });
//functions
function claimId(name){
    let buffer = name.replace(/ /g, '_');
    if(buffer.slice(-1) !== '_'){
        return buffer + '_' + Math.floor(Math.random() * (1000000000));
    }else{
        return buffer + Math.floor(Math.random() * (1000000000));
    }
}

module.exports = router;