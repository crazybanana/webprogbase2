const express = require("express");
const cloudinary = require('cloudinary');
const multer = require('multer');
const crypto = require('crypto');
var auth = require('basic-auth');
const router = express.Router();

const User = require("../../../../model/User");

let upload  = multer({ storage: multer.memoryStorage() });

cloudinary.config({
    cloud_name: 'ddbvrf8lf',
    api_key: '544275783565819',
    api_secret: 'c6e5peIp3c3cjwLh5U7opTSSAps'
});

router.use(express.static("public"));

router.get('/',
    async(req, res) =>{
        let credentials = auth(req);
        if(!credentials || credentials.name !== 'Administrator' || credentials.pass !== '1234567890'){
            res.send({'error': 'not authorized'});
        }else{
            try{
                await User.paginate({}, {limit: 5}, function(err, result){
                    if(err){
                        res.send({'error': 'storage error'})
                    }else{
                        res.send(result);                        
                    }
                });
            }catch(err){
                res.send({'error': 'catch error'});
            }
        }
    });

router.post('/add',
    upload.any(),
    async(req, res) =>{
        let credentials = auth(req);
        if(!credentials || credentials.name !== 'Administrator' || credentials.pass !== '1234567890'){
            res.send({'error': 'not authorized'});
        }else{
            try{
                let username, email, phone, password;
                req.body.username ? (username = req.body.username) : res.send({'error': 'username is required'});
                req.body.email    ? (email = req.body.email) : res.send({'error': 'email is required'});
                req.body.phone    ? (phone = req.body.phone) : res.send({'error': 'phone is required'});
                req.body.password ? (password = req.body.password) : res.send({'error': 'password is required'});
                let passwordHash = sha512(password, serverSalt).passwordHash;

                let user = await (new User({
                    username,
                    email,
                    phone,
                    password: passwordHash,
                    role: "user"
                }));

                await user.save(function (err) {
                    if(err){
                        res.send({'error': 'storage save error'});
                        return;
                    }
                });

                res.send(user);
            }catch(err){
                res.send({'error': 'catch error'});                
            }
        }
    })

//functions
const serverSalt = "ufhdgu*Y%YB$*ruihg";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
}

module.exports = router;