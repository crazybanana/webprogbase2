const express = require("express");
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cloudinary = require('cloudinary');
const multer = require('multer');
const cities = require("all-the-cities");
const router = express.Router();

const Storage = require("../model/Storage");

const upload  = multer();
const country = cities.filter(city =>{
    return city.country.match('UA');
});
const cityBuff = country.map(a => a.name);

cloudinary.config({
    cloud_name: 'ddbvrf8lf',
    api_key: '544275783565819',
    api_secret: 'c6e5peIp3c3cjwLh5U7opTSSAps'
});

router.use(express.static("public"));
router.use(session({
    secret: "webtj947tb$*$)974e",
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());

router.get("/",
    async (req, res) => {
        try {
            let storage = await Storage.find();
            res.render("./../views/goods", {object: storage, user: req.user, countries: cityBuff});
        }catch (err){
            if(err) throw err;
        }
    });

router.get("/add",
    async (req, res) => {
        if(req.user){
            res.render("./../views/add", {user: req.user, countries: cityBuff});
        }else{
            res.redirect("/profile/login");
        }
    });

router.post("/addgoods",
    upload.any(),
    async (req, res) => {
        let name = req.body.goodsName;
        let city = req.body.goodsCity;
        let category = req.body.goodsCategory;
        let condition = req.body.goodsCondition;
        let about = req.body.goodsAbout;
        let image;
        let imageID;
        let id = claimId(name);

        try {
            if(req.files.length > 0){
                let base64String = await ('data:image/png;base64,' + (req.files[0].buffer).toString('base64'));
                await cloudinary.v2.uploader.upload(base64String, function(error, result) {
                    result ? (image = result.url) : null;
                    result ? (imageID = result.public_id) : null;
                });
            }else {            
                image = null;
                imageID = null;
            }

            let storage = await (new Storage({
                id,
                name,
                city,
                category,
                condition,
                about,
                image,
                imageID,
                ownerID: req.user._id
            }));

            await storage.save(function (err) {
                if(err) throw(err);
            });
            res.redirect("/");
        }catch (err){
            console.log(err);
            res.sendStatus(404);
        }
    });

router.post("/delete",
    upload.any(),
    async (req, res) => {
        try {
            let storage = await Storage.findOne({id: req.body.id}, async (err, storage) => {
                if(err) throw err;
                if(storage.imageID !== null){
                    await cloudinary.v2.uploader.destroy(storage.imageID);                    
                }
                await storage.remove(function (err) {
                    if(err) throw err;
                    res.redirect("/goods");
                });
            });
        }catch (err){
            res.sendStatus(400);
        }
    });

router.get("/:good_id",
    async (req, res) => {
        try {
            let storage = await Storage.findOne({id: req.params.good_id});
            if(req.user){
                res.render("./../views/good", {object: storage, user: req.user, role: req.user.role, countries: cityBuff});
            }else{
                res.render("./../views/good", {object: storage, user: req.user, role: "", countries: cityBuff});
            }
        }catch (err){
            res.sendStatus(404);
        }
    });

//functions
function claimId(name){
    let buffer = name.replace(/ /g, '_');
    if(buffer.slice(-1) !== '_'){
        return buffer + '_' + Math.floor(Math.random() * (1000000000));
    }else{
        return buffer + Math.floor(Math.random() * (1000000000));
    }
}

module.exports = router;