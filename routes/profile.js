const express = require("express");
const crypto = require('crypto');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const multer = require('multer');
const cities = require("all-the-cities");
const router = express.Router();

const User = require('./../model/User');
const Storage = require("../model/Storage");

const upload  = multer();
const country = cities.filter(city =>{
    return city.country.match('UA');
});
const cityBuff = country.map(a => a.name);

router.use(express.static("public"));

//passport
passport.serializeUser(function (user, done) {
    done(null, user._id);
});

passport.deserializeUser(function (_id, done) {
    let user = User.find();
    User.findOne({'_id': _id})
        .then(user => {
            if (!user) done("No user des");
            else done(null, user);
        });
});

passport.use(new LocalStrategy(
    function (username, password, done) {
        let passwordHash = sha512(password, serverSalt).passwordHash;
        User.findOne({username: username, password: passwordHash})
            .then(user =>{
                if(!user) return done("No user loc", false);
                return done(null, user);
            });
    }
));

router.use(cookieParser());
router.use(session({
    secret: "webtj947tb$*$)974e",
    resave: false,
    saveUninitialized: true
}));
router.use(passport.initialize());
router.use(passport.session());

router.get("/",
    async (req, res) =>{
        if(req.user){
            let storage = await Storage.find({ownerID: req.user._id});
            res.render('profile', {
                object: storage,
                user: req.user,
                countries: cityBuff
            })
        }else{
            res.render("login", {user: req.user, countries: cityBuff});
        }
    });

router.get('/admin',
    checkAdmin,
    async (req, res) => {
        let allUsers = await User.find();
        res.render('admin', {
            user: req.user,
            allUsers: allUsers, 
            countries: cityBuff
        });
    });

router.get('/logout',
    (req, res) => {
        req.logout();
        res.redirect('login');
    });

router.get('/register',
    async (req, res) => {
        if(req.user){
            res.redirect("/profile");
        }else{
            res.render("register", {user: req.user, countries: cityBuff});
        }
    });

router.get("/login",
    async (req, res) => {
        if(req.user){
            res.redirect("/profile");
        }else if(!req.user || (req.user.ban === "true")){
            res.render("login", {user: req.user, countries: cityBuff});
        }else{
            res.redirect("/profile");
        }
    });

router.post('/login',
    passport.authenticate('local'),
    (req, res) => {
        res.redirect('/profile');
    });

router.post("/register",
    upload.any(),
    async (req, res) => {
        let username = req.body.username;
        let email = req.body.email;
        let phone = req.body.phone;
        let password = req.body.password;
        let passwordHash = sha512(password, serverSalt).passwordHash;

        try{
            let user = new User({
                username,
                email,
                phone,
                password: passwordHash,
                role: "user"
            });
            await user.save(function (err) {
                if(err) throw err;
            });
            res.redirect("login");
        }catch (err){
            res.sendStatus(500);
        }
    });

//functions
function checkAdmin(req, res, next) {
    if (!req.user) return res.render('errors/401', {user: req.user, countries: cityBuff});
    if (req.user.role !== 'admin') return res.render('errors/401', {user: req.user, countries: cityBuff});
    next();
}

function checkAuth(req, res, next) {
    if (!req.user) return res.redirect("/login");
    next();
}

const serverSalt = "ufhdgu*Y%YB$*ruihg";

function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
}

module.exports = router;